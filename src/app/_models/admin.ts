export class Admin {
	id: number;
	first_name: string;
	last_name: string;
	email: any;
	contact: any;
	password: string;
}
