export class Timetable {
	id: any;
	programme_id: string;
	programme_name: string;
	year: string;
	semester: string;
	file: any;
	post_date: any;
	file_name: any;
}
