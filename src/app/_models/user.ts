﻿export class User {
	id: number;
	name: string;
	registration_no: string;
	student_no: string;
	student_course_name: string;
	student_course_code: string;
	email: any;
	contact: any;
	office_no: any;
	user_type: boolean;
	is_student: boolean;
	is_lecturer: boolean;
	is_admin: boolean;
}
