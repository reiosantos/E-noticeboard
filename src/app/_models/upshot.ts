export class Upshot {
	id: any;
	title: string;
	subtitle: string;
	description: string;
	event_date: any;
	post_date: any;
	user_id: any;
	file: any;
	file_name: any;
	user_name: any;
}
