export class Programme {
	id: number;
	name: string;
	code: string;
}
